# frozen_string_literal: true

require_relative "lib/flip_feature/version"

Gem::Specification.new do |spec|
  spec.name = "flip_feature"
  spec.version = FlipFeature::VERSION
  spec.authors = ["ritika gakher"]
  spec.email = ["ritika.gakher@builder.ai"]

  spec.summary = "Gem to enable/disable features on runtime"
  spec.description = "Gem to enable/disable features on runtime"
  spec.homepage = "https://gitlab.com/ritikarails/flip_feature"
  spec.license = "MIT"
  spec.required_ruby_version = ">= 2.6.0"

  spec.metadata["allowed_push_host"] = "Set to your gem server 'https://example.com'"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/ritikarails/flip_feature"
  spec.metadata["changelog_uri"] = "https://gitlab.com/ritikarails/flip_feature"
  spec.add_dependency 'redis', '>= 4.0', '< 6'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  # spec.files = Dir.chdir(__dir__) do
  #   `git ls-files -z`.split("\x0").reject do |f|
  #     (File.expand_path(f) == __FILE__) ||
  #       f.start_with?(*%w[bin/ test/ spec/ features/ .git appveyor Gemfile])
  #   end

  # end

  spec.files = Dir["lib/**/*.rb"]
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  # Uncomment to register a new dependency of your gem
  # spec.add_dependency "example-gem", "~> 1.0"

  # For more information and examples about making a new gem, check out our
  # guide at: https://bundler.io/guides/creating_gem.html
end
