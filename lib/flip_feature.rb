# frozen_string_literal: true
require 'redis'
require_relative "flip_feature/version"

module FlipFeature
  class Error < StandardError; end
  # Your code goes here...
  def set_feature(name, value)
    REDIS_CLIENT.set(name, value)
  end
  
  def get_feature(name)
    REDIS_CLIENT.get(name)
  end
  
  def enabled?(name)
    REDIS_CLIENT.get(name) == "on" ? true : false
  end
end

